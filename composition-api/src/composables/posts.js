import { ref, computed } from 'vue';

export default function() {
    const posts = ref([
        { id: 1, title: 'Lorem ipsum', isPublished: false },
        { id: 2, title: 'Dolor sit amet', isPublished: false },
        { id: 3, title: 'Foor bar baz', isPublished: false },
      ]);

    const publishedPosts = computed(
        () => posts.value.filter(post => post.isPublished)
      );
  
      function publishPosts() {
        posts.value = posts.value.map(post => {
          post.isPublished = true;
          return post
        });
      }

    return {
        publishPosts,
        publishedPosts,
    }
}
