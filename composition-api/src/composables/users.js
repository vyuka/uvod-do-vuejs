import { ref } from 'vue';

export default function() {
    const users = ref([]);

    function loadUsers() {
      users.value = [
        'John Doe',
        'Karel Novák',
        'Jan Novotný'
      ];
    }
    
    return { users, loadUsers }
}
