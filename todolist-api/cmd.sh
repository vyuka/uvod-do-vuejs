#!/bin/bash

# ================== #
# ===  COMMANDS  === #
# ================== #

case "$1" in
    "dc")
        shift 1

        docker-compose --version &> /dev/null
        if [[ $? == "0" ]]; then
          dc="docker-compose"
        fi

        docker compose version &> /dev/null
        if [[ $? == "0" ]]; then
          dc="docker compose"
        fi

        if [[ -z "${dc}" ]]; then
            echo "Neither docker-compose nor docker compose seems to be installed."
            exit 1
        fi

        ${dc} $@
    ;;

    "configure")
        [[ -f .env ]] || cp .env.example .env
        [[ -f docker-compose.yml ]] || cp docker/docker-compose.yml.example docker-compose.yml
        uid=$(id -u)
        sed -i '' "s/www:1000/www:${uid}/g" docker-compose.yml
    ;;

    "attach-to-shell")
        SERVICE="$2"
        USER="$3"

        [[ -z ${SERVICE} ]] && SERVICE="php"
        [[ -z ${USER} ]] && USER=$UID

        ./cmd.sh dc exec -u $USER ${SERVICE} sh
    ;;

    "restart-service")
        if [[ -z "$2" ]]; then
            # Restart whole app
            ./cmd.sh dc down && ./cmd.sh dc up -d
        else
            # Restart only specified service
            ./cmd.sh dc stop "$2"
            ./cmd.sh dc rm -f "$2"
            ./cmd.sh dc up -d
        fi
    ;;

    "key-generate")
      key=$(./cmd.sh dc exec --user $UID php php artisan key:generate --show --no-ansi)
      ./cmd.sh dc exec --user $UID -T php php <<EOM
        <?php
          \$key = trim("$key");
          \$file = 'docker-compose.yml';
          \$dc = file_get_contents(\$file);
          \$newContent = str_replace(
            'APP_KEY: ""',
            sprintf('APP_KEY: "%s"', \$key),
            \$dc
          );
          file_put_contents(\$file, \$newContent);
EOM
    ;;

    *)
        echo "Unknown command ${@}"
    ;;
esac
