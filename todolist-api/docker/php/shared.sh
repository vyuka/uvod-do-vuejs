#!/bin/sh

echo "Installing shared dependencies" \
  && lit preset laravel \
  && install-php-extensions \
    openswoole-4.12.0
