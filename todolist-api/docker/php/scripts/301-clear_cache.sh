#!/bin/sh

if [ -d vendor ]; then
    echo "Clearing view cache..."
    php artisan view:clear
else
    echo "Can not clear cache. First install composer packages."
fi
