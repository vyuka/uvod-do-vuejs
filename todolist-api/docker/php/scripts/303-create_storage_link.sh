#!/bin/sh

PUBLIC_STORAGE_PATH="./public/storage"

if [ -d "${PUBLIC_STORAGE_PATH}" ]; then
    echo "Public storage dir already exists, deleting..."
    rm -rf "${PUBLIC_STORAGE_PATH}"
fi

if [ -d vendor ]; then
    echo "Creating storage link"
    php artisan storage:link
else
    echo "Can not create storage link. First install composer packages."
fi
