#!/bin/sh

if [ -d vendor ]; then
    echo "Running database migrations..."
    php artisan migrate --force
else
    echo "Can not run database migrations. First install composer packages."
fi
