<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property int $id
 * @property string $name
 * @property int $priority
 * @property int $order
 * @property Carbon $due_date
 * @property Carbon $completed_at
 * @property Carbon $archived_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @method static TodoItem create(array $data)
 */
class TodoItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'priority',
        'order',
        'due_date',
        'completed_at',
        'archived_at',
    ];
}
