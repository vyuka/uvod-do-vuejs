<?php

namespace App\Http\Controllers;

use App\Models\TodoItem;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Symfony\Component\HttpFoundation\Response as SymfonyResponse;

class TodoItemController extends Controller
{
    public function index(Request $request): JsonResponse
    {
        $todos = TodoItem::query();

        $showCompleted = (bool)$request->get('show_completed', true);

        if (!$showCompleted) {
            $todos->whereNull('completed_at');
        }

        if ($request->has('order_by')) {
            $orderBy = $request->get('order_by');
            $allowedOptions = ['name', 'created_at', 'priority', 'due_date', 'completed_at'];

            abort_if(
                !in_array($orderBy, $allowedOptions),
                code: SymfonyResponse::HTTP_UNPROCESSABLE_ENTITY
            );

            $direction = $request->get('direction', 'asc');
            $direction = $direction === 'desc' ? 'desc' : 'asc';

            $todos->orderBy($orderBy, $direction);
        }

        if ($request->has('priority')) {
            $todos->where('priority', (int)$request->get('priority'));
        }

        // /api/todos?order_by=name/created_at&direction=asc/desc

        return response()->json([
            'todos' => $todos->get()
        ]);
    }

    public function store(Request $request): JsonResponse
    {
        return response()->json(TodoItem::create($request->all()));
    }

    public function show(TodoItem $todoItem)
    {
        //
    }

    public function update(Request $request, TodoItem $todo): JsonResponse
    {
        $todo->update($request->all());

        return response()->json($todo);
    }

    public function destroy(TodoItem $todo): Response
    {
        $todo->delete();

        return response(status: SymfonyResponse::HTTP_NO_CONTENT);
    }
}
