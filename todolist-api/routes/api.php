<?php

declare(strict_types=1);

use App\Http\Controllers\TodoItemController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', static fn () => response()->json([
    'name' => config('app.name'),
    'version' => config('app.version'),
]));

Route::resource('todos', TodoItemController::class);
