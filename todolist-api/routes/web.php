<?php

declare(strict_types=1);

use Illuminate\Support\Facades\Route;

Route::get('swagger', static fn () => view('swagger'));
